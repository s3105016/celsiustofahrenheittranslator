package nl.utwente.di.bookQuote;

import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;

/** Example of a Servlet that gets an ISBN number and returns the book price
 */

public class TemperatureConvert extends HttpServlet {
 	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private TempConverter tempConverter;
	
    public void init() throws ServletException {
    	tempConverter = new TempConverter();
    }	
	
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String title = "Convert temperature";
    
    // Done with string concatenation only for the demo
    // Not expected to be done like this in the project
    out.println("<!DOCTYPE HTML>\n" +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                		"</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +              
                "  <P>temperature: " +
                   request.getParameter("temperature") + "\n" +
                "  <P>temperatureInFahrenheit: " +
                   tempConverter.celsiusToFahrenheit(request.getParameter("temperature")) +
                "</BODY></HTML>");
  }
}
