package nl.utwente.di.bookQuote;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestConverter {

    @Test
    public void testBook1 ( ) throws Exception {
        TempConverter tempConverter = new TempConverter( ) ;
        double price = tempConverter.celsiusToFahrenheit("0") ;
        Assertions.assertEquals(32.0, price, 0.0, "tempinfahrenheit") ;
    }
}
